#!/usr/bin/env bash

#Script to export godot-python projects, until it is fully supported by the Godot exporter

EXPORT_PRESET=$1
OUTPUT_ARCHIVE=$2
PROJECT_NAME=$(basename $OUTPUT_ARCHIVE .zip)
TEMP_DIR="/tmp/godot_python_export"
EXPORT_FOLDER="$TEMP_DIR/$PROJECT_NAME"
EXPORTED_FILE="$EXPORT_FOLDER/export.zip"

GODOT_VERSION=$(basename $(godot --version) .arch_linux)
TEMPLATE_FOLDER="~/.local/share/godot/templates/$GODOT_VERSION"
WINDOWS_EXECUTABLE="$TEMPLATE_FOLDER/windows_64_release.exe"
LINUX_EXECUTABLE="$TEMPLATE_FOLDER/linux_x11_64_release"

GODOT_PYTHON_FOLDER="addons/pythonscript"
GODOT_PYTHON_FOLDER_DESTINATION="$EXPORT_FOLDER/$GODOT_PYTHON_FOLDER"


#Creating a temporary folder
mkdir "$TEMP_DIR"
#Creating a folder for the current export in the temporary folder
mkdir "$EXPORT_FOLDER"

#Exporting the project to the temporary folder
godot --no-window --export-pack "$EXPORT_PRESET" "$EXPORTED_FILE"

#Extracting the zip archive, then removing it.
unzip -o "$EXPORTED_FILE" -d "$EXPORT_FOLDER"
rm  "$EXPORTED_FILE"

#Copy the python addon folder
if [[ "$EXPORT_PRESET" == "Windows" ]]; then
  cp -r "./$GODOT_PYTHON_FOLDER/windows-64" "$GODOT_PYTHON_FOLDER_DESTINATION"

elif [[ "$EXPORT_PRESET" == "Linux/X11" ]]; then
  cp -r "./$GODOT_PYTHON_FOLDER/x11-64" "$GODOT_PYTHON_FOLDER_DESTINATION"

else
  cp -r "./$GODOT_PYTHON_FOLDER" "$GODOT_PYTHON_FOLDER_DESTINATION"
fi

#Copy and renames the template executables
if [[ "$EXPORT_PRESET" == "Windows" ]]; then

  cp -r "$WINDOWS_EXECUTABLE" "$EXPORT_FOLDER/$PROJECT_NAME.exe"

elif [[ "$EXPORT_PRESET" == "Linux/X11" ]]; then

  cp -r "$LINUX_EXECUTABLE" "$EXPORT_FOLDER/$PROJECT_NAME"
  chmod +x "$EXPORT_FOLDER/$PROJECT_NAME"

else

  cp -r "$WINDOWS_EXECUTABLE" "$EXPORT_FOLDER"
  cp -r "$LINUX_EXECUTABLE" "$EXPORT_FOLDER"

fi

#Adding everything to the output archive
zip -r "$OUTPUT_ARCHIVE" "$EXPORT_FOLDER"
