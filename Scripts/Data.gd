extends Node

signal data_processed(id)
signal data_loaded()
signal data_exported()
signal all_data_prossed()

onready var geocoder := $Utils/Geocode

var Address_column_name := "ADRESSE"
var values : Dictionary = {}

func get_number_of_addresses():
	return values[self.Address_column_name].size()

func get_number_of_addresses_processed():
	var counter:= 0
	for i in self.values["status"]:
		if (self.values["status"][i] == Status.Status.FAILED) or (self.values["status"][i] == Status.Status.VALID):
			counter += 1
	return counter

func get_number_of_localisation_error():
	var counter:= 0
	for i in self.values["status"]:
		if self.values["status"][i] == Status.Status.FAILED:
			counter += 1
	return counter

func get_column_names():
	return self.values.keys()

func extract_from(file):
	print("Extracting Data from %s" % file)
	self.values = JSON.parse($Utils/Csv_to_json.convert(file)).result
	self.values["latitude"] = {}
	self.values["longitude"] = {}
	self.values["status"] = {}
	emit_signal("data_loaded")

func export_to(file):
	var exported_data = self.values.duplicate(true)
	exported_data.erase("status")
	$Utils/Json_to_csv.convert(JSON.print(exported_data),file)
	emit_signal("data_exported")

func process_address(id):
	var address = self.values[self.Address_column_name][id]
	match geocoder.get_geocode(address):
		true:
			self.values["status"][id] = Status.Status.VALID
		false:
			self.values["status"][id] = Status.Status.FAILED

	self.values["latitude"][id] = geocoder.latitude
	self.values["longitude"][id] = geocoder.longitude
	emit_signal("data_processed",id)

func process_all_address(_user_data):
	for address in self.values[self.Address_column_name]:
		process_address(address)
		OS.delay_msec(1000)
	emit_signal("all_data_prossed")
