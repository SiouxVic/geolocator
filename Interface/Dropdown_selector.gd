extends MarginContainer
onready var dropdown : OptionButton = $HBoxContainer/VBoxContainer/Dropdown

func set_data(array:Array = []):
	for i in array:
		dropdown.add_item(i)

func get_selected_data():
	return dropdown.get_item_text(dropdown.get_selected_id())
