extends "res://Interface/Dropdown_selector.gd"

var possible_column_names := [
	"ADRESSE POSTALE",
	"ADRESSES POSTALES",
	"ADRESSE",
	"ADRESSES"
]

func set_data(array:Array = []):
	.set_data(array)
	self._detect_address_column()

func _detect_address_column():
	for i in self.dropdown.get_item_count():
		var column_name : String = self.dropdown.get_item_text(dropdown.get_item_id(i)).to_upper()
		for test_name in possible_column_names:
			if test_name in column_name:
				dropdown.select(dropdown.get_item_id(i))
