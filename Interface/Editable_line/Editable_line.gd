tool
extends MarginContainer

export var editable := false setget set_editable
onready var line := $VBoxContainer/HBoxContainer/Line
onready var update_button := $VBoxContainer/HBoxContainer/Update_button
onready var label := $VBoxContainer/Label

func set_editable(is_editable):
	editable = is_editable
	
	label.visible = editable
	update_button.visible = editable
	line.editable = editable

func _ready():
	pass
