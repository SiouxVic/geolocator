extends MarginContainer

onready var filepath := $VBoxContainer/HBoxContainer/FilePath

func file_exist():
	var file = File.new()
	
	if file.file_exists(filepath.text):
		return true
	return false

func _ready():
	pass

func _on_Select_file_Button_pressed():
	$CanvasLayer/FileDialog.popup()


func _on_FileDialog_file_selected(path):
	filepath.text = path
