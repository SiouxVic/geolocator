extends PanelContainer

onready var line_index := "0" setget set_line_index

onready var Address_text : LineEdit = $MarginContainer/GridContainer/Left/Address_data.line
onready var Address_data := $MarginContainer/GridContainer/Left/Address_data
onready var Location_name := $MarginContainer/GridContainer/Left/Location_name
onready var latitude := $MarginContainer/GridContainer/CenterContainer/Middle/MarginContainer/HBoxContainer/Latitude/VBoxContainer/Data
onready var longitude := $MarginContainer/GridContainer/CenterContainer/Middle/MarginContainer/HBoxContainer/Longitude/VBoxContainer/Data
onready var Data_node : Node = get_tree().get_nodes_in_group("Data")[0]

onready var Ignore_button := $MarginContainer/GridContainer/Right/MarginContainer/CenterContainer/VBoxContainer/Compare_coordinates_button
onready var Cancel_button := $MarginContainer/GridContainer/Right/MarginContainer/CenterContainer/VBoxContainer/Ignore_error_button
onready var Compare_button := $MarginContainer/GridContainer/Right/MarginContainer/CenterContainer/VBoxContainer/Cancel_ignore_button

func set_line_index(data_id := "0"):
	line_index = data_id
	update_scene()

func update_scene():
	Location_name.text = self.line_index
	latitude.text = String(Data_node.values["latitude"][self.line_index])
	longitude.text = String(Data_node.values["longitude"][self.line_index])
	Address_text.text = Data_node.values[Data_node.Address_column_name][self.line_index]
	self._update_status()

func _update_status():
	match Data_node.values["status"][self.line_index]:
		Status.Status.FAILED:
			Address_data.editable = true
		Status.Status.VALID:
			Address_data.editable = false
		Status.Status.IGNORED:
			pass


func _on_Compare_coordinates_button_pressed():
	#Check if the coordinates where found
	match Data_node.values["status"][self.line_index]:
		Status.Status.FAILED:
			OS.shell_open("https://www.google.com/maps?hl=fr&q=%s" % self.Address_text.text.percent_encode())
		Status.Status.VALID:
			var coordinates_request := "%s %s" % [latitude.text,longitude.text]
			OS.shell_open("https://www.google.com/maps?hl=fr&q=%s" % coordinates_request.percent_encode())
		Status.Status.IGNORED:
			pass
