extends MarginContainer

onready var data : Node = get_tree().get_nodes_in_group("Data")[0]
onready var processed := $HBoxContainer/Address_Processed_label
onready var errors := $HBoxContainer/Errors

var processed_text := "Adresse traitées: %s/%s"
var errors_text := "Erreurs de localisation: %s"

func update():
	self.visible = true
	processed.text = processed_text % [data.get_number_of_addresses_processed(), data.get_number_of_addresses()]
	if data.get_number_of_localisation_error() > 0:
		errors.visible = true
		errors.text = errors_text % data.get_number_of_localisation_error()
	else:
		errors.visible = false

func _on_Data_data_processed(_id):
	update()


func _on_Data_data_loaded():
	update()
