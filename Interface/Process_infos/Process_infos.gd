extends MarginContainer

var Address_info_scene := preload("res://Interface/Adress_info/Address_infos.tscn")
onready var Data_node : Node = get_tree().get_nodes_in_group("Data")[0]
onready var Failed := $PanelContainer/MarginContainer/TabContainer/Erreurs
onready var Valid := $"PanelContainer/MarginContainer/TabContainer/Traitées"

func _ready():
	pass

func clear():
	Failed.clear()
	Valid.clear()

func _on_Data_data_processed(id):
	print("Processing data!")
	if Data_node.values["status"][id] == Status.Status.FAILED:
		Failed.add_address_info(id)
	else:
		Valid.add_address_info(id)
