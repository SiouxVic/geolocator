extends ScrollContainer

var Address_info_scene := preload("res://Interface/Adress_info/Address_infos.tscn")

onready var container:= $VBoxContainer

func _ready():
	pass

func clear():
	for i in container.get_children():
		i.queue_free()

func add_address_info(id):
	for i in container.get_children():
		if i.line_index == id:
			return i.set_line_index(id)
	var info := Address_info_scene.instance()
	container.add_child(info)
	info.set_line_index(id)
