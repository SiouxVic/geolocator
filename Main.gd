extends Control

onready var Address_dropdown := $PanelContainer/VBoxContainer/Options/HBoxContainer/MarginContainer2/VBoxContainer2/Address_column_selector
onready var Input_filepath := $PanelContainer/VBoxContainer/Options/HBoxContainer/MarginContainer/VBoxContainer/Input_file_selector
onready var output_filepath := $PanelContainer/VBoxContainer/Options/HBoxContainer/MarginContainer/VBoxContainer/Output_file_selector
onready var save_button := $PanelContainer/VBoxContainer/Footer/HBoxContainer/Save_button
var t

func _ready():
	get_tree().connect("files_dropped",self,"_on_files_dropped")

func _on_files_dropped(files:PoolStringArray,_screen:int):
	var file : String =  files[0]
	if file.get_extension().to_upper() in "CSV":
		Input_filepath.filepath.text = file 
		$Data.extract_from(file)

func _on_Data_data_loaded():
	Address_dropdown.set_data($Data.get_column_names())


func _on_Start_button_pressed():
	$PanelContainer/VBoxContainer/Body.clear()
	$Data.Address_column_name = Address_dropdown.get_selected_data()
	t = Thread.new()
	t.start($Data,"process_all_address",null)


func _on_Save_button_pressed():
	$CanvasLayer/Save_FileDialog.popup_centered()


func _on_Cancel_button_pressed():
	get_tree().quit()


func _on_FilePath_text_changed(_new_text):
	$Data.extract_from(Input_filepath.filepath.text)


func _on_Data_all_data_prossed():
	save_button.disabled = false
	t.wait_to_finish()


func _on_FileDialog_file_selected(path):
	$Data.extract_from(path)


func _on_Save_FileDialog_file_selected(path):
	$Data.export_to(path)


func _on_Data_data_processed(id):
	print("%s processed!" % id)
