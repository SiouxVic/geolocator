from godot import exposed, export
from godot import *
from unidecode import unidecode
import geopy

USER_AGENT = "GeoLocator.py"
Banfr_geolocator = geopy.geocoders.BANFrance(user_agent=USER_AGENT,timeout=5)

@exposed
class Geocode(Node):
	
	def are_adresses_similar(self,address1="", address2=""):
		clean_address1 = unidecode(address1.upper())
		clean_address2 = unidecode(address2.upper())
		print("Checking: {} is equal to {} = {}".format(clean_address1,
														clean_address2,
														clean_address1.find(clean_address2)))
		return clean_address1.find(clean_address2) == 0
	
	def get_geocode(self,address="", check_output_address=True):
		coordinates = ()
		address = str(address)
		self.latitude = 0.0
		self.longitude = 0.0
		try:
			location = Banfr_geolocator.geocode(address)
			coordinates = (location.latitude, location.longitude)
			print("Search: {}, found: {}".format(address, location.address))
		except Exception as e:
			print("Not found: {}".format(address))
			print(e)
			return False

		if check_output_address and not self.are_adresses_similar(address, location.address):
			print("{} does not match {}".format(address, location.address))
			return False
		self.latitude = coordinates[0]
		self.longitude = coordinates[1]
		return True

	def _ready(self):
		self.latitude = 0.0
		self.longitude = 0.0
