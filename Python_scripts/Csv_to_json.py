from godot import exposed, export
from godot import *
import pandas

@exposed
class Csv_to_json(Node):

	def _ready(self):
		pass

	def convert(self,file):
		print(file)
		try:
			data = pandas.read_csv(str(file),index_col=0)
			return data.to_json()
		except e:
			print("Could not load parse the data!")
		return False
