from godot import exposed, export
from godot import *
import pandas
import json

@exposed
class Json_to_csv(Node):

	def _ready(self):
		pass

	def convert(self,file,output):
		data = pandas.DataFrame(json.loads(str(file)))
		data.to_csv(str(output))
